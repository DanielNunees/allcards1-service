<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Billings;

class BillingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $customer_id)
    {
        $payment_token = $request->input("payment_token");
        $schedule = $request->input("schedule");

        $billing = new Billings();
        $billing->customer_id = $customer_id;
        $billing->schedule_id = $schedule['id'];
        $billing->status = "active";


        $service_fee = $schedule['course']['service_fee'];
        $course_fee = $schedule['course']['price'];
        $amount =  $service_fee * $course_fee;


        $billing->payment_method = "credit card";
        $billing->service_fee = $service_fee;
        $billing->course_fee = $course_fee;
        $billing->amount = $amount;
        $billing->payment_token = $payment_token["id"];
        //return $billing;
        $billing->save();
        return $billing;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
