<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;

class AuthController extends Controller
{
    public static function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string',
            'password' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        $token = response()->json(compact('token'));

        $user = User::where('email', $request->input('email'))->first();
        return $token;
    }

    public function sigin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        $user = User::where('email', $request->input('email'))->first();


        if (is_null($user)) {
            $response = UserController::newUser($request);
        } else {
            return response()->json("Usuário ja cadastrado", 500);
        }
        return $response;
    }

    public function me()
    {
        if (JWTAuth::parseToken()->authenticate()) {
            return JWTAuth::parseToken()->authenticate();
        } else {
            return false;
        }
    }
}
