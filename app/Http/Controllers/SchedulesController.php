<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CourseSchedule;
use Illuminate\Support\Carbon;

class SchedulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $course_schedule = CourseSchedule::all();
        foreach ($course_schedule as $key => $value) {
            $value->course;
        }
        return $course_schedule;
    }

    public function schedules()
    {
        $course_schedule = CourseSchedule::orderBy('start_time')
            ->get()
            ->groupBy(function ($val) {
                return Carbon::parse($val->start_time)->format('d M y');
            });
        foreach ($course_schedule as $key => $value) {
            foreach ($value as $aux) {
                $aux->course;
                $aux->course->school;
                $aux->course->school->address;
            }
        }

        return ($course_schedule->values());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $start_time = substr($request->start_time, 0, strpos($request->start_time, '('));
        $end_time = substr($request->end_time, 0, strpos($request->end_time, '('));

        $start_time_mysql = date("Y-m-d H:i:s", strtotime($request->start_time));
        $end_time_mysql = date("Y-m-d H:i:s", strtotime($end_time));
        $schedule = new CourseSchedule();
        $schedule->course_id = $request->input('course_id');
        $schedule->start_time = $start_time_mysql;
        $schedule->end_time = $end_time_mysql;
        //$schedule->save();
        return $schedule;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
