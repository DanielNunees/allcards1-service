<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Charge;

class BookingController extends Controller
{
    function __construct()
    { }

    public function newBooking(Request $request)
    {
        try {
            Stripe::setApiKey("sk_test_p9TgeSVx4fedWCJALacXc3j100fL6K6p6p");
            $customer = new CustomerController();
            $new_customer = $customer->store($request);

            $addressController = new AddressController();
            $address = $request->input("address");
            $new_address = $addressController->storeAddress($address, null, $new_customer->id);

            $billingController = new BillingsController();
            $new_billing = $billingController->store($request, $new_customer->id);


            $token = $request->input("payment_token");
            $amount = 14595;
            $description = "Test number 1";

            $charge = Charge::create([
                'amount' => $amount,
                'currency' => 'aud',
                'description' => $description,
                'source' => $token['id'],
            ]);
            return response()->json("Payment successful", 200);
        } catch (\Exception $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
        }
    }

    
}
