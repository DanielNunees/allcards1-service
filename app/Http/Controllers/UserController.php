<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use JWTAuth;

class UserController extends Controller
{

    /**
     * UserController constructor.
     */
    public function __construct()
    { }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public static function newUser(Request $request)
    {
        try {
            User::create([
                'email' => $request->input('email'),
                'name' => $request->input('name'),
                'password' => bcrypt($request->input('password')),
            ]);
            return response()->json("Usuário Cadastrado com Sucesso", 200);
        } catch (\Exception $ex) {
            return response()->json($ex->getMessage(), 500);
        }
    }
}
