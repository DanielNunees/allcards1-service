<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    protected $fillable = ['name','school_id'];

    public function school(){
        return $this->hasOne("App\Models\School","id","school_id");
    }
}
