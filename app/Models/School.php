<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable = ['name','phone','description'];

    public function address(){
        return $this->hasOne("App\Models\Address","school_id");
    }

}
