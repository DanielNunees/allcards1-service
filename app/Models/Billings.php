<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Billings extends Model
{
    protected $fillable = [
    'customer_id',
    'schedule_id',
    'status',
    'payment_method',
    'service_fee',
    'course_fee',
    'payment_token',
    'amount']; 
}
