<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        if (!Schema::hasTable('customers')) {
            Schema::create('customers', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('first_name')->nullable(false);
                $table->string('last_name')->nullable(false);
                $table->string('gender')->nullable(false);
                $table->string('email')->nullable(false);
                $table->string('phone')->nullable(false);
                $table->dateTime('birthday');
                $table->timestamps();
                $table->softDeletes();
            });
        } else {
            Schema::table('customers', function (Blueprint $table) {
                // if (!Schema::hasColumn('users')) {
                // }

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
