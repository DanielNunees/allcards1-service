<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('billings')) {
            Schema::create('billings', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('customer_id');
                $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');;

                $table->unsignedBigInteger('schedule_id');
                $table->foreign('schedule_id')->references('id')->on('course_schedules')->onDelete('cascade');;

                $table->string('status');
                $table->string('payment_method');
                $table->string('service_fee');
                $table->string('course_fee'); 
                $table->text('payment_token');
                $table->unsignedDecimal('amount', 8, 2);
                $table->timestamps();
                $table->softDeletes();
            });
        } else {
            Schema::table('billings', function (Blueprint $table) {
                // if (!Schema::hasColumn('users')) {
                // }

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billings');
    }
}
