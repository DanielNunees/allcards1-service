<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        if (!Schema::hasTable('courses')) {
            Schema::create('courses', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->double('service_fee');
                $table->double('price')->nullable(false);
                $table->unsignedBigInteger('school_id');
                $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');;
                $table->timestamps();
                $table->softDeletes();
            });
        } else {
            Schema::table('courses', function (Blueprint $table) {
                // if (!Schema::hasColumn('users')) {
                // }

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
