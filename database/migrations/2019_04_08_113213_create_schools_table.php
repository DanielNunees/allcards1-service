<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('schools')) {
            Schema::create('schools', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name')->nullable(false);
                $table->string('email')->nullable(true);
                $table->string('phone')->nullable(false);
                $table->string('img_url')->nullable(true);
                $table->string('description')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        } else {
            Schema::table('schools', function (Blueprint $table) {
                // if (!Schema::hasColumn('users')) {
                // }

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
