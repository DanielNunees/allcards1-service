<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */


Route::post('login', 'AuthController@login');
Route::post('sigin', 'AuthController@sigin');
Route::post('logout', 'AuthController@logout');
Route::post('refresh', 'AuthController@refresh');
//Route::get('me', 'AuthController@me');

Route::group(['middleware' => ['jwt.verify']], function () {
    //Follow and Unfollow Routes
    Route::resource('courses', 'CourseController');
    Route::resource('schools', 'SchoolController');
    Route::resource('billings', 'BillingsController');
    Route::resource('course/schedule', 'SchedulesController');
    //Route::resource('address', 'SchedulesController');
});
Route::get('course/schedules', 'SchedulesController@schedules');
Route::post('booking',"BookingController@newBooking");
